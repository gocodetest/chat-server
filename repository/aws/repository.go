// Package aws contains the implementation of aws s3 repository.
package aws

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.com/gocodetest/chat-server/model"
	"gitlab.com/gocodetest/chat-server/stream"
)

// StreamIdleTimeout defines the time during which the one stream will be open.
const StreamIdleTimeout = time.Second * 15

// RepositoryConfig defines the aws s3 repository configuration params.
type RepositoryConfig struct {
	Context     context.Context
	Bucket      string
	Region      string
	ID          string
	Secret      string
	BgErrorChan chan error
}

// NewRepository returns a new instance of the amazon s3 repository.
func NewRepository(cfg RepositoryConfig) *Repository {
	s3cfg := aws.NewConfig()
	s3cfg.Region = aws.String(cfg.Region)
	s3cfg.Credentials = credentials.NewStaticCredentials(cfg.ID, cfg.Secret, "")
	sess := session.Must(session.NewSession(s3cfg))
	return &Repository{
		bucket:    cfg.Bucket,
		sess:      sess,
		ctx:       cfg.Context,
		streams:   make(map[string]*stream.Stream),
		bgErrChan: cfg.BgErrorChan,
	}
}

// Repository provides the methods doe interaction with the Amazon s3.
type Repository struct {
	bucket    string
	sess      *session.Session
	ctx       context.Context
	mx        sync.Mutex
	streams   map[string]*stream.Stream
	bgErrChan chan error
}

// Send sends the message to the aws s3 bucket as a part of the stream.
func (s3 *Repository) Send(ctx context.Context, msg model.Message) error {
	msgDate := time.Unix(msg.Timestamp/1000, 0).Format("2006-01-02")
	fileName := fmt.Sprintf("chat/%s/content_logs_%s_%d", msgDate, msgDate, msg.ClientID)
	s3.mx.Lock()
	s := s3.streams[fileName]
	if s == nil || s.Closed() {
		s = stream.NewStream(stream.Config{
			IdleTimeout: StreamIdleTimeout,
			Context:     s3.ctx,
			BgErrorChan: s3.bgErrChan,
		})
		s3.streams[fileName] = s
		go s3.runUploader(fileName, s)
	}
	s3.mx.Unlock()
	data, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("error while marshaling the message to json: %v", err)
	}
	// add new line to the data
	data = append(data, 10)
	_, err = s.Write(data)
	if err != nil {
		return fmt.Errorf("error while writing data to stream: %w", err)
	}
	return nil
}

func (s3 *Repository) runUploader(fileName string, s io.Reader) {
	uploader := s3manager.NewUploader(s3.sess)
	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket:          aws.String(s3.bucket),
		Key:             aws.String(fileName),
		Body:            s,
		ContentType:     aws.String("application/json"),
		ContentEncoding: aws.String("gzip"),
	})
	if err != nil {
		s3.bgErrChan <- fmt.Errorf("error while uploading file: %w", err)
	}
}
