// Package repository is in charge of interaction with the remote message storage.
//go:generate mockgen -source=service.go -destination=../mock/repository.go -package=mock -mock_names Service=Repository
package repository

import (
	"context"

	"gitlab.com/gocodetest/chat-server/model"
)

// Service describes the interface of the remote repository.
type Service interface {
	Send(ctx context.Context, msg model.Message) error
}
