// Package azure contains the implementation of azure blob storage repository.
package azure

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"sync"
	"time"

	"github.com/Azure/azure-storage-blob-go/azblob"
	"gitlab.com/gocodetest/chat-server/model"
	"gitlab.com/gocodetest/chat-server/stream"
)

// StreamIdleTimeout defines the time during which the one stream will be open.
const StreamIdleTimeout = time.Second * 15

// BufferSize defines the max buffer size of the uploader.
const BufferSize = 5 * 1024 * 1024

// MaxBuffers defines the max buffers count of the uploader reader.
const MaxBuffers = 10

// RepositoryConfig defines the azure blob storage repository configuration params.
type RepositoryConfig struct {
	Context     context.Context
	Account     string
	Key         string
	Container   string
	BgErrorChan chan error
}

// NewRepository returns a new instance of the azure blob storage repository.
func NewRepository(cfg RepositoryConfig) *Repository {
	credential, err := azblob.NewSharedKeyCredential(cfg.Account, cfg.Key)
	if err != nil {
		panic(fmt.Errorf("error while creating azure credentials: %w", err))
	}
	URL, err := url.Parse(fmt.Sprintf("https://%s.blob.core.windows.net/%s", cfg.Account, cfg.Container))
	if err != nil {
		panic(fmt.Errorf("error while pasing azure container url: %w", err))
	}
	p := azblob.NewPipeline(credential, azblob.PipelineOptions{})
	cURL := azblob.NewContainerURL(*URL, p)
	return &Repository{
		cURL:      cURL,
		ctx:       cfg.Context,
		streams:   make(map[string]*stream.Stream),
		bgErrChan: cfg.BgErrorChan,
	}
}

// Repository provides the methods for interaction with the azure blob storage.
type Repository struct {
	cURL      azblob.ContainerURL
	ctx       context.Context
	mx        sync.Mutex
	streams   map[string]*stream.Stream
	bgErrChan chan error
}

// Send sends the message to the azure blob storage.
func (r *Repository) Send(ctx context.Context, msg model.Message) error {
	msgDate := time.Unix(msg.Timestamp/1000, 0).Format("2006-01-02")
	fileName := fmt.Sprintf("content_logs_%s_%d", msgDate, msg.ClientID)
	r.mx.Lock()
	s := r.streams[fileName]
	if s == nil || s.Closed() {
		s = stream.NewStream(stream.Config{
			IdleTimeout: StreamIdleTimeout,
			Context:     r.ctx,
			BgErrorChan: r.bgErrChan,
		})
		r.streams[fileName] = s
		go r.runUploader(fileName, s)
	}
	r.mx.Unlock()
	data, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("error while marshaling the message to json: %v", err)
	}
	// add new line to the data
	data = append(data, 10)
	_, err = s.Write(data)
	if err != nil {
		return fmt.Errorf("error while writing data to stream: %w", err)
	}
	return nil
}

func (r *Repository) runUploader(fileName string, s io.Reader) {
	blobURL := r.cURL.NewBlockBlobURL(fileName)
	_, err := azblob.UploadStreamToBlockBlob(context.Background(), s, blobURL, azblob.UploadStreamToBlockBlobOptions{
		BufferSize: BufferSize,
		MaxBuffers: MaxBuffers,
		BlobHTTPHeaders: azblob.BlobHTTPHeaders{
			ContentEncoding: "gzip",
		},
	})
	if err != nil {
		r.bgErrChan <- fmt.Errorf("error while uploading file: %w", err)
	}
}
