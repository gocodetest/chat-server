# Chat Server

## Installation
Compiled binary depends only on the environment variables described below.\
Application can be wrapped with systemd script optionally.

## Environment Variables
- HTTP_ADDR - host:port for HTTP API (**required**)
- REPOSITORY_TYPE - `aws` and `azure` values are accepted (**required**)
- AWS_BUCKET - aws bucket name (**required for aws**)
- AWS_REGION - aws region (**required for aws**)
- AWS_ID - aws Access Key ID (**required for aws**)
- AWS_SECRET - aws Secret Access Key (**required**)
- AZURE_ACCOUNT - Azure Blob Storage Account (**required for azure**)
- AZURE_KEY - Azure Blob Storage Access Key (**required for azure**)
- AZURE_CONTAINER - Azure Blob Storage Container (**required for azure**)

## Routes
POST /message - send chat message

## Benchmarks & Profiling Result
Benchmarks were run on the developer's machine with Intel Core i5.\
Client was launched with 1000 gorutines and 1 billion messages in the queue.

### Amazon s3
- max RAM consumption on the server is less than 80 Mb.
- server accepts approx. 13500 queries per second.

### Azure Blob Storage
- max RAM consumption on the server is less than 60 Mb.
- server accepts approx. 13000 queries per second.

## Logged time
It took 19 hours to implement the server at this moment and state.