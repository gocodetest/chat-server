// Package controller contains the application controller interface and it's implementation.
//go:generate mockgen -source=service.go -destination=../mock/controller.go -package=mock -mock_names Service=Controller
package controller

import (
	"context"

	"gitlab.com/gocodetest/chat-server/model"
)

// Service describes the interface of the controller.
type Service interface {
	ReceiveMessage(ctx context.Context, msg model.Message) error
}
