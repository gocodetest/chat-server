package controller

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gocodetest/chat-server/mock"
	"gitlab.com/gocodetest/chat-server/model"
)

func TestController_ReceiveMessage(t *testing.T) {
	ctx := context.Background()
	mockCTRL := gomock.NewController(t)
	msg := model.Message{
		Text:      "test",
		ContentID: 1,
		ClientID:  2,
		Timestamp: 3,
	}
	repoError := errors.New("repository error")

	tt := []struct {
		name          string
		repoError     error
		expectedError error
	}{
		{
			name:          "repository error",
			repoError:     fmt.Errorf("%w: test repository error", repoError),
			expectedError: repoError,
		},
		{
			name: "success",
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			repository := mock.NewRepository(mockCTRL)
			repository.EXPECT().Send(ctx, msg).Return(tc.repoError)
			ctrl := NewController(repository)
			err := ctrl.ReceiveMessage(ctx, msg)
			assert.True(
				t,
				errors.Is(err, tc.expectedError),
				fmt.Sprintf("expected error '%v'; got '%v'", tc.repoError, err),
			)
		})
	}
}
