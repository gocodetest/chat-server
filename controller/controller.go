package controller

import (
	"context"

	"gitlab.com/gocodetest/chat-server/model"
	"gitlab.com/gocodetest/chat-server/repository"
)

// NewController returns a new instance of the application controller.
func NewController(r repository.Service) Controller {
	return Controller{repo: r}
}

// Controller is the basic implementation of the application controller.
type Controller struct {
	repo repository.Service
}

// ReceiveMessage processes the incoming message.
func (c Controller) ReceiveMessage(ctx context.Context, msg model.Message) error {
	return c.repo.Send(ctx, msg)
}
