package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/gocodetest/chat-server/controller"
	"gitlab.com/gocodetest/chat-server/repository"
	"gitlab.com/gocodetest/chat-server/repository/aws"
	"gitlab.com/gocodetest/chat-server/repository/azure"
	"gitlab.com/gocodetest/chat-server/router"
)

// ShutdownDelay defines the delay before shutting down the application.
// It is needed for letting the file streams be closed properly.
const ShutdownDelay = time.Second * 5

const (
	// RepositoryAWS contains the name of aws repository in order to handle environment variable.
	RepositoryAWS = "aws"
	// RepositoryAzure contains the name of azure repository in order to handle environment variable.
	RepositoryAzure = "azure"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer func() {
		cancel()
		time.Sleep(ShutdownDelay)
	}()

	bgErrors := make(chan error)
	go func() {
		for err := range bgErrors {
			log.Println(err)
		}
	}()

	var repo repository.Service
	repoType := os.Getenv("REPOSITORY_TYPE")
	switch repoType {
	case RepositoryAWS:
		repo = aws.NewRepository(aws.RepositoryConfig{
			Context:     ctx,
			Bucket:      os.Getenv("AWS_BUCKET"),
			Region:      os.Getenv("AWS_REGION"),
			ID:          os.Getenv("AWS_ID"),
			Secret:      os.Getenv("AWS_SECRET"),
			BgErrorChan: bgErrors,
		})
	case RepositoryAzure:
		repo = azure.NewRepository(azure.RepositoryConfig{
			Context:     ctx,
			Account:     os.Getenv("AZURE_ACCOUNT"),
			Key:         os.Getenv("AZURE_KEY"),
			Container:   os.Getenv("AZURE_CONTAINER"),
			BgErrorChan: bgErrors,
		})
	default:
		panic(fmt.Errorf("invalid repository type '%s'", repoType))
	}

	ctrl := controller.NewController(repo)
	r := router.NewRouter(ctrl, bgErrors)
	addr := os.Getenv("HTTP_ADDR")
	if err := fasthttp.ListenAndServe(addr, r.Handler); err != nil {
		panic(fmt.Errorf("attempt to listen http on '%s' addr failed: %w", addr, err))
	}
}
