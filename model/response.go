package model

// Response represents the basic HTTP API response.
type Response struct {
	Data   interface{} `json:"data,omitempty"`
	Error  string      `json:"error,omitempty"`
	Status int         `json:"-"`
}
