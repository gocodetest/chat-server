// Package model contains the entities that are used across the application.
package model

// Message describes the structure of a single chat message.
type Message struct {
	Text      string `json:"text"`
	ContentID int64  `json:"content_id"`
	ClientID  int64  `json:"client_id"`
	// Timestamp contains the unix time with millisecond precision.
	Timestamp int64 `json:"timestamp"`
}
