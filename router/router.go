package router

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/valyala/fasthttp"
	"gitlab.com/gocodetest/chat-server/controller"
	"gitlab.com/gocodetest/chat-server/model"
)

// NewRouter returns a new instance of the HTTP API router.
func NewRouter(ctrl controller.Service, bgErrors chan error) Router {
	return Router{ctrl: ctrl, bgErrors: bgErrors}
}

// Router is the basic implementation of the HTTP API router.
type Router struct {
	ctrl     controller.Service
	bgErrors chan error
}

// Handler handles the HTTP request and call the proper route.
func (r Router) Handler(ctx *fasthttp.RequestCtx) {
	var resp model.Response
	switch string(ctx.Path()) {
	case "/message":
		resp = r.Message(ctx)
	default:
		resp = r.NotFound(ctx)
	}
	respData, err := json.Marshal(resp)
	if err != nil {
		r.bgErrors <- fmt.Errorf("unable to marshal API response: %w", err)
		ctx.Response.SetStatusCode(http.StatusInternalServerError)
		ctx.Response.SetBodyString("Internal Server Error")
		return
	}
	ctx.Response.SetStatusCode(resp.Status)
	ctx.Response.SetBody(respData)
}

// Message receives and processes the message from client.
func (r Router) Message(ctx *fasthttp.RequestCtx) model.Response {
	var msg model.Message
	if err := json.Unmarshal(ctx.Request.Body(), &msg); err != nil {
		return model.Response{
			Error:  "Error while parsing body: " + err.Error(),
			Status: http.StatusBadRequest,
		}
	}
	if err := r.ctrl.ReceiveMessage(ctx, msg); err != nil {
		r.bgErrors <- fmt.Errorf("error while processing message: %w", err)
		return model.Response{
			Error:  "Internal Server Error",
			Status: http.StatusInternalServerError,
		}
	}
	return model.Response{
		Data:   "Message accepted successfully",
		Status: http.StatusOK,
	}
}

// NotFound returns 404 error.
func (r Router) NotFound(ctx *fasthttp.RequestCtx) model.Response {
	return model.Response{
		Error:  fmt.Sprintf("Page %s is not found", string(ctx.Path())),
		Status: http.StatusNotFound,
	}
}
