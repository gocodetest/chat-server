// Package router contains the HTTP API router interface and it's implementation.
package router

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/gocodetest/chat-server/model"
)

// Service describes the interface of the HTTP API router.
type Service interface {
	Handler(ctx *fasthttp.RequestCtx)
	Message(ctx *fasthttp.RequestCtx) model.Response
	NotFound(ctx *fasthttp.RequestCtx) model.Response
}
