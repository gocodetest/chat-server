package router

import (
	"errors"
	"net/http"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"gitlab.com/gocodetest/chat-server/mock"
	"gitlab.com/gocodetest/chat-server/model"
)

func TestRouter_Message(t *testing.T) {
	mockCTRL := gomock.NewController(t)

	type ctrlParams struct {
		err error
	}

	tt := []struct {
		name string
		body string
		ctrl *ctrlParams
		resp model.Response
	}{
		{
			name: "invalid json",
			body: "invalid_json",
			resp: model.Response{
				Error:  "Error while parsing body: invalid character 'i' looking for beginning of value",
				Status: http.StatusBadRequest,
			},
		},
		{
			name: "controller error",
			body: "{}",
			ctrl: &ctrlParams{err: errors.New("controller error")},
			resp: model.Response{
				Error:  "Internal Server Error",
				Status: http.StatusInternalServerError,
			},
		},
		{
			name: "success",
			body: "{}",
			ctrl: &ctrlParams{},
			resp: model.Response{
				Data:   "Message accepted successfully",
				Status: http.StatusOK,
			},
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			ctx := &fasthttp.RequestCtx{}
			ctx.Request.SetRequestURI("/message")
			ctx.Request.SetBodyString(tc.body)
			ctrl := mock.NewController(mockCTRL)
			if tc.ctrl != nil {
				ctrl.EXPECT().ReceiveMessage(ctx, gomock.Any()).Return(tc.ctrl.err).Times(2)
			}
			r := NewRouter(ctrl, make(chan error, len(tt)*2))
			resp := r.Message(ctx)
			assert.Equal(t, tc.resp, resp)

			// test routing
			r.Handler(ctx)
			assert.Equal(t, tc.resp.Status, ctx.Response.StatusCode())
		})
	}
}

func TestRouter_NotFound(t *testing.T) {
	r := NewRouter(nil, make(chan error, 2))
	ctx := &fasthttp.RequestCtx{}
	ctx.Request.SetRequestURI("/invalid-page")
	resp := r.NotFound(ctx)
	expResp := model.Response{
		Error:  "Page /invalid-page is not found",
		Status: http.StatusNotFound,
	}
	assert.Equal(t, expResp, resp)

	// test routing
	r.Handler(ctx)
	assert.Equal(t, expResp.Status, ctx.Response.StatusCode())
}
