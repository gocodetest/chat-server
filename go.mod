module gitlab.com/gocodetest/chat-server

go 1.13

require (
	github.com/Azure/azure-storage-blob-go v0.8.0
	github.com/aws/aws-sdk-go v1.30.7
	github.com/golang/mock v1.4.3
	github.com/klauspost/compress v1.10.4 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/valyala/fasthttp v1.9.0
)
