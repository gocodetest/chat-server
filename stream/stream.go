// Package stream contains the streams for uploading data to the external storage.
package stream

import (
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"sync"
	"time"
)

// Config describes the configuration params of the stream.
type Config struct {
	IdleTimeout time.Duration
	Context     context.Context
	BgErrorChan chan error
}

// NewStream return a new instance of the stream.
func NewStream(cfg Config) *Stream {
	reader, writer := io.Pipe()
	gz := gzip.NewWriter(writer)
	s := Stream{
		ctx:         cfg.Context,
		reader:      reader,
		writer:      writer,
		gz:          gz,
		idleTimeout: cfg.IdleTimeout,
		bgErrChan:   cfg.BgErrorChan,
	}
	go s.checkIdle(cfg.IdleTimeout)
	return &s
}

// Stream implements the io.Reader and io.Writer interfaces.
// It provides possibility to stream gzipped data with idle timeout.
type Stream struct {
	// mutex is required in order to write only one message simultaneously
	sync.Mutex
	ctx         context.Context
	reader      *io.PipeReader
	writer      *io.PipeWriter
	gz          *gzip.Writer
	closed      bool
	idle        *time.Timer
	idleTimeout time.Duration
	bgErrChan   chan error
}

// Write writes specified bytes to the stream.
func (s *Stream) Write(p []byte) (n int, err error) {
	s.Lock()
	defer s.Unlock()
	go s.checkIdle(s.idleTimeout)
	return s.gz.Write(p)
}

// Read reads the data from the stream.
func (s *Stream) Read(p []byte) (n int, err error) {
	n, err = s.reader.Read(p)
	return n, err
}

// Close closes the stream.
func (s *Stream) Close() error {
	s.Lock()
	defer s.Unlock()
	if s.closed {
		return nil
	}
	s.closed = true
	if err := s.gz.Close(); err != nil {
		return err
	}
	if err := s.writer.Close(); err != nil {
		return err
	}
	return nil
}

// Closed returns true if the stream is closed.
func (s *Stream) Closed() bool {
	s.Lock()
	defer s.Unlock()
	return s.closed
}

func (s *Stream) checkIdle(timeout time.Duration) {
	s.Lock()
	if s.idle != nil {
		s.idle.Reset(timeout)
		s.Unlock()
		return
	}
	s.idle = time.NewTimer(timeout)
	s.Unlock()

	var err error
	select {
	case <-s.idle.C:
		err = s.Close()
	case <-s.ctx.Done():
		err = s.Close()
	}
	if err != nil {
		s.bgErrChan <- fmt.Errorf("error while closing stream: %w", err)
	}
}
