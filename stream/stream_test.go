package stream

import (
	"context"
	"io/ioutil"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestStream(t *testing.T) {
	const (
		closeTypeManually = iota
		closeTypeAuto
		closeTypeContext
	)

	in := [][]byte{[]byte("hello"), []byte("world")}
	out := []byte{
		31, 139, 8, 0, 0, 0, 0, 0, 0, 255, 202, 72, 205, 201, 201, 47, 207,
		47, 202, 73, 1, 4, 0, 0, 255, 255, 173, 32, 235, 249, 10, 0, 0, 0,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	tt := []struct {
		name      string
		cfg       Config
		closeType int
	}{
		{
			name:      "close manually",
			cfg:       Config{IdleTimeout: time.Second, Context: context.Background()},
			closeType: closeTypeManually,
		},
		{
			name:      "close automatically",
			cfg:       Config{IdleTimeout: time.Second, Context: context.Background()},
			closeType: closeTypeAuto,
		},
		{
			name:      "close by context",
			cfg:       Config{IdleTimeout: time.Second, Context: ctx},
			closeType: closeTypeContext,
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			s := NewStream(tc.cfg)
			var res []byte
			var resError error
			var resMx sync.Mutex

			go func() {
				resMx.Lock()
				res, resError = ioutil.ReadAll(s)
				resMx.Unlock()
			}()

			for _, msg := range in {
				if _, err := s.Write(msg); err != nil {
					t.Fatalf("unexpected error while writing: %v", err)
				}
			}

			switch tc.closeType {
			case closeTypeManually:
				err := s.Close()
				if err != nil {
					t.Fatalf("unexpected error while closing: %v", err)
				}
			case closeTypeAuto:
				time.Sleep(tc.cfg.IdleTimeout + time.Second)
			case closeTypeContext:
				cancel()
				time.Sleep(time.Second)
			}

			if !s.Closed() {
				t.Fatalf("stream expected to be closed")
			}

			resMx.Lock()
			if resError != nil {
				t.Fatalf("unexpected error while reading: %v", resError)
			}
			assert.Equal(t, out, res)
			resMx.Unlock()
		})
	}
}
